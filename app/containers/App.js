import React, { Component, PropTypes } from 'react';
import {
  AppBar
} from 'material-ui';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render() {
    return (
      <div style={{height: '100%'}}>
        <AppBar title='Stream Dashboard' />
        <div style={{marginTop: '0.75rem', height: '100%'}}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
