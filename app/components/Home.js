import React, { Component } from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid';
import {Chat} from './chat';
import styles from './Home.css';

export default class Home extends Component {
  render() {
    const exmoAvatar = 'https://static-cdn.jtvnw.net/jtv_user_pictures/extrememoderation-profile_image-bf123ef1ef77c54e-300x300.png';
    const messages = [
      {
        id: 1,
        username: 'ExtremeModeration',
        text: 'Hello there!',
        avatar: exmoAvatar
      }, {
        id: 2,
        username: 'SomeViewer',
        text: 'Nice stream you have here.'
      }, {
        id: 3,
        username: 'ExtremeModeration',
        text: 'Thanks! Glad you dropped by!',
        avatar: exmoAvatar
      }, {
        id: 4,
        username: 'SomeOtherViewer',
        text: 'Hey!, I\'m here too!',
      }, {
        id: 5,
        username: 'SomeViewer',
        text: 'Yes, we are both here...'
      }, {
        id: 6,
        username: 'ExtremeModeration',
        text: 'So you are!',
        avatar: exmoAvatar
      }, {
        id: 7,
        username: 'ExtremeModeration',
        text: 'Hello viewers, here is a nice long paragraph of text. How does it look when it has to wrap around?',
        avatar: exmoAvatar
      }
    ];

    return (
      <Grid className={styles.container}>
        <Row style={{height: '100%'}}>
          <Col xs={8} style={{height: '100%'}}>
            <h3>Main area</h3>
          </Col>

          <Col xs={4} style={{height: '100%'}}>
            <Chat messages={messages} />
          </Col>
        </Row>
      </Grid>
    );
  }
}
