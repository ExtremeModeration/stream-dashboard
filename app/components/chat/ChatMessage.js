/**
 * Created by steve on 4/8/16.
 */

import React from 'react';
import {Avatar} from 'material-ui';
import {Row, Col} from 'react-flexbox-grid';
import style from './ChatMessage.css';

export default function ChatMessage(props) {
  let avatarSource = 'https://static-cdn.jtvnw.net/jtv_user_pictures/xarth/404_user_150x150.png';

  if (props.avatar && props.avatar !== '') {
    avatarSource = props.avatar;
  }

  const avatar = (<Avatar src={avatarSource} />);

  return (
    <div className={style.container}>
      <Row>
        <Col xs={2}>
          {avatar}
        </Col>

        <Col xs={10}>
          <div className={style.username}>
            {props.username}
          </div>
          <div className={style.body}>
            {props.text}
          </div>
        </Col>
      </Row>
    </div>
  );
}
