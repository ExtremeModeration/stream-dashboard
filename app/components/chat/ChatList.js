/**
 * Created by steve on 4/8/16.
 */

import React from 'react';
import style from './ChatList.css';
import ChatMessage from './ChatMessage';

export default function ChatList(props) {
  return (
    <div className={style.container}>
      {props.messages.map((message) => {
        return (
          <ChatMessage
            key={'message-' + message.id}
            {...message} />
        )
      })}
    </div>
  )
}
