/**
 * Created by steve on 4/8/16.
 */

import React from 'react';
import {Col, Row} from 'react-flexbox-grid';
import {Paper} from 'material-ui';
import style from './Chat.css';
import ChatList from './ChatList';

export default function Chat(props) {
  return (
    <div className={style.container}>
      <Paper style={{height: '100%'}}>
        <Row style={{height: '100%'}}>
          <Col xs={12} style={{height: '100%'}}>
            <ChatList messages={props.messages} />
          </Col>
        </Row>

        <Row style={{position: 'relative', bottom: '7rem'}}>
          <Col xs={10}>input...</Col>
          <Col xs={2}>Button</Col>
        </Row>
      </Paper>
    </div>
  )
}
